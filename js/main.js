const apiKeys = {
  prod: {
    UNBXD_API_KEY_CUSTOM: 'sites',

    UNBXD_SITE_KEY_GYM_US: 'gymboree-com800681569331157',
    UNBXD_API_KEY_GYM_US: 'a1a16a3789804b726230012acdb74613',

    UNBXD_API_KEY_TCP_US: '8870d5f30d9bebafac29a18cd12b801d',
    UNBXD_SITE_KEY_TCP_US: 'childrensplace-com702771523455856',

    UNBXD_API_KEY_TCP_CA: '35594164b71a71a4acf546f74dc23688',
    UNBXD_SITE_KEY_TCP_CA: 'ca-childrensplace-com702771526591884',

    UNBXD_API_KEY_GYM_CA: '2d5855cc3851909b2b0863198e8aed5f',
    UNBXD_SITE_KEY_GYM_CA: 'ca-gymboree-com800681569331172',
  },
  stage: {
    UNBXD_API_KEY_CUSTOM: 'sites',
    UNBXD_SITE_KEY_GYM_US: 'qa-gymboree-com800681562072138',
    UNBXD_API_KEY_GYM_US: '4d75a00877d6ff7c434a90e71c8588bb',

    UNBXD_API_KEY_TCP_US: '7aa6e28c2c204b582f31a959d289f5ae',
    UNBXD_SITE_KEY_TCP_US: 'stage-childrensplace-com702771524059437',

    UNBXD_API_KEY_TCP_CA: 'b4d99f7007e749d4f6a9eef87c0163a9',
    UNBXD_SITE_KEY_TCP_CA: 'stage-ca-childrensplace-com702771526591951',

    UNBXD_API_KEY_GYM_CA: '01b34560057a75a3836032db63fbb6d3',
    UNBXD_SITE_KEY_GYM_CA: 'qa-ca-gymboree-com800681565760713'
  },
};

const productsGrid = document.getElementById('productsGrid');

$(function () {
  $('.categoryWrapper,#menu').hide()
  var $radios = $('input:radio[name=stockinformation]');
  if ($radios.is(':checked') === false) {
    $radios.filter('[value=InStock]').prop('checked', true);
  }
  $('#uploadcmc').hide();

  $('#search-results').bind('keyup change', function (ev) {
    // pull in the new value
    const searchTerm = $(this).val();
    // remove any old highlighted terms
    removeHighlight();

    // disable highlighting if empty
    if (searchTerm && searchTerm.length > 2) {
      // highlight the new term
      selectProductTiles(searchTerm);
    } else {
      $('.results-count').hide();
    }
  });

  $('#btnFind').click(function (e) {
    getProducts();
  });

  $("#back2Top").click(function (event) {
    event.preventDefault();
    $("html, body").animate({
      scrollTop: 0
    }, "slow");
    return false;
  });

  $('#txtCategory').on('keypress', function (e) {
    if (e.which === 13) {
      getProducts();
      e.preventDefault();
    }
  });

  randombg();
});

function getProducts() {
  let category = $('#txtCategory').val().trim();
  const searchType = $('input[name=search]:checked').val();
  if (searchType === 'cid') {
    category ? getUnbxdData(category) : alert('Please enter Category Id!');
  } else {
    category ? getUnbxdData(category) : alert('Please enter category keyword!');
  }
}

function selectProductTiles(term) {
  const productsList = $(productsGrid).children();

  productsList.each(function (i, e) {
    const name = $(e).attr('data-productname');
    const searchTerm = new RegExp(term, 'gi');
    if (name.match(searchTerm)) {
      $(e).addClass('multiple-sortable-selected').addClass('shake-tile');
    }
  })
  const selectedTilesCount = $(productsGrid).children('.multiple-sortable-selected').length;
  if (selectedTilesCount) {
    $('.results-count').show();
    $('#results').html(selectedTilesCount);
  } else {
    $('.results-count').hide();
  }
}

function removeHighlight() {
  const productsList = $(productsGrid).children();
  productsList.each(function (i, e) {
    $(e).removeClass('multiple-sortable-selected').removeClass('shake-tile');
  })
}

function moveAllBegining() {
  $($(productsGrid).children('.multiple-sortable-selected')).prependTo($(productsGrid));
  $('div').removeClass('multiple-sortable-selected');
}

function moveAllEnd() {
  $($(productsGrid).children('.multiple-sortable-selected')).appendTo($(productsGrid));
  $('div').removeClass('multiple-sortable-selected');
}

function unSelectAll() {
  $('div').removeClass('multiple-sortable-selected');
}

$(document.body).click(function (e) {
  $('.dropdown-content').hide();
});

productNameTemplate = (name) => '<span class="product-name">' + name + '</span>';

toolTipText = (text) => '<span class="tooltiptext">' + text + '</span>';

productId = (pid) => '<p class="product-price">PID: ' + pid + '</p>';

newarrival = (date) => '<p class="newArrivalTag TCP">Arrival Date : '+date+'</p>'

dotsIcon = (name) => '<img class="dots-icon" src="images/dots-icon.png" onclick="openTileMenu(event, this);" data-val="' + name + '" />';

function getImageTemplate(path) {
  path = 'https://www.childrensplace.com/wcsstore/GlobalSAS/images/tcp/products/500/' + path + '.jpg';
  return $('<img />').attr({
    'src': path,
    'alt': 'image',
    'class': 'product-image'
  })
}

function categoryPath(catPath) {
  let html = '';
  if (catPath && catPath.length) {
    for (let i = 0; i < catPath.length; i++) {
      html += '<span class="product-level">' + catPath[i] + '</span>';
    }
  }
  return '<div class="product-category">' + html + '</div>';
}

function productPrice(item) {
  let html = '';
  html += '<span class="product-price">$' + item.min_offer_price + '</span>';
  html += item.min_list_price ? '<span class="product-price"> Was: $' + item.min_list_price + '</span>' : '';
  return '<div class="price-wrapper">' + html + '</div>';
}

function updateEnvironment() {
  categoryId = $('#txtCategory').val().trim();
  categoryId && getUnbxdData(categoryId);
}

function updateBrand() {
  categoryId = $('#txtCategory').val().trim();
  categoryId && getUnbxdData(categoryId);
}


function updateCountry() {
  categoryId = $('#txtCategory').val().trim();
  categoryId && getUnbxdData(categoryId);
}

function getProductsOrder() {
  let csvArray = [];
  const productsList = $(productsGrid).children();
  productsList.each(function (i, e) {
    const pid = $(e).attr('data-productid');
    csvArray.push([pid, ++i]);
  })
  return csvArray;
}

function openTileMenu(e, element) {
  const id = $(element).attr('data-val');
  const pos = $(element).offset();
  $('.dropdown-content').css({
    top: pos.top + 20,
    left: pos.left + 10,
    position: 'absolute'
  });
  $('.dropdown-content').toggle();
  $('.dropdown-content').attr('data-id', id);
  e.stopPropagation();

}

function createCSV() {
  const env = $('#environment').val();
  const csvArray = getProductsOrder();
  let csvContent = "data:text/csv;charset=utf-8," + 'PID,Position\n' + csvArray.map(e => e.join(",")).join("\n");
  var encodedUri = encodeURI(csvContent);
  var link = document.createElement("a");
  link.setAttribute("href", encodedUri);
  link.setAttribute("download", env + "-products-order.csv");
  document.body.appendChild(link); // Required for FF
  link.click();
}

function getUnbxdData(category) {
  let url;
  const searchType = $('input[name=search]:checked').val();
  if (searchType === 'cid') {
    url = getUnbxdUrl(searchType) + '"' + category + '"';
  } else {
    url = getUnbxdUrl(searchType) + category;
  }
  $(productsGrid).html('');
  $.get(url).done(function (data) {
    debugger;
    const stockflag = $('#stock').is(':checked');
    if (data && data.response && data.response.products && data.response.products.length) {
      const products = data.response.products;
      products.forEach((item , i )=> {
        let div = $('<div>').attr({
          'class': 'grid-square',
          'data-productid': item.productid,
          'data-productname': item.product_name,
        });

        //item.variants.v_qty
        for (i = 0; i < item.variants.length; i++) {
          if(item.variants[i].v_qty > 0 ) {
            $(div).attr('data-inventorystatus', 'available').removeClass('unavailable')
            break;
          } else {
            $(div).attr('data-inventorystatus', 'unavailable').addClass('unavailable')
          }
         
        }

        var splitAttribute = item.list_of_attributes.split(';');
        var myval = splitAttribute.reduce((acc, value) => {
          const curr = value.split(':');
          acc[curr[0]] = curr[1];
          return acc;
        }, {})
        var date1 = myval['TCPProductArrivalDateUSStore'];
       // var date2 = new Date();
        //var timeDiff = Math.abs(date2.getTime() - date1.getTime());
       // var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
       // var newarrivalDiffDate = diffDays < 80 ? newarrival() : '';

        // if(!stockflag && item.TCPOutOfStockFlagUSStore){
        $(div).append(dotsIcon(item.imagename)).append(getImageTemplate(item.imagename)).append(productNameTemplate(item.product_name)).append(productPrice(item)).append(productId(item.productid)).append(newarrival(date1));
        $(div).appendTo($(productsGrid));
        // }
        //  if(stockflag && !item.TCPOutOfStockFlagUSStore) {
        //   $(div).append(dotsIcon(item.imagename)).append(getImageTemplate(item.imagename)).append(productNameTemplate(item.product_name)).append(productPrice(item)).append(productId(item.productid));
        //   $(div).appendTo($(productsGrid));
        //  }
      });
      applySorting();
      addShakeEffect();
      $('#btnCreateCSV').show();
      $('.search-results-wrapper, .select-control').show();
      //$('body').removeClass('img-body').addClass('white-body');
      $('.products-wrapper').addClass('white-body');
      $('.no-products-text').hide();
    } else {
      $('#btnCreateCSV').hide();
      $('.search-results-wrapper, .select-control').hide();
      $('.products-wrapper').removeClass('white-body');
      $('.no-products-text').show();
    }
  }).fail(function () {
    console.log('Error');
    $('#btnCreateCSV').hide();
  });
}

function applySorting() {
  $(productsGrid).multipleSortable({
    items: 'div.grid-square',
    connectWith: '#productsGrid',
    container: '.products-wrapper',
    orientation: 'horizontal',
    keepSelection: false,
    start: function (e) {
      clearTimeout(timeout);
    },
    stop: function (e) {
      clearTimeout(timeout);
    }
  });
}

function moveToBegining(element) {
  let id = $(element).parent().attr('data-id');
  const div = $('[data-val=' + id + ']').parent();
  $(div).prependTo($(productsGrid));
  $('.dropdown-content').hide();
}

function moveToEnd(element) {
  let id = $(element).parent().attr('data-id');
  const div = $('[data-val=' + id + ']').parent();
  $(div).appendTo($(productsGrid));
  $('.dropdown-content').hide();
}

function hideShowCMCbtn() {
  if ($('#cid').is(':checked')) {
    //  $('.categoryWrapper,#menu').show();
  }
  $('#cid').is(':checked') && $('#outofstock').is(':checked') ? $('#uploadcmc').show() : $('#uploadcmc').hide()
}

function getUnbxdUrl(searchType) {
  const env = $('#environment').val();
  const brandName = $('#brand').val();
  const locale = $('#locale').val();
  const apiKey = 'UNBXD_API_KEY_' + brandName + '_' + locale;
  const siteKey = 'UNBXD_SITE_KEY_' + brandName + '_' + locale;
  const oosList = !$('#stock').is(':checked') ? 'promotion=false' : '';

  let url;
  if (searchType === 'cid') {
    url = 'https://search.unbxd.io/' + apiKeys[env][apiKey] + '/' + apiKeys[env][siteKey] + '/category?start=0&rows=200&variants=true&' + oosList + 'variants.count=100&&version=V2&fields=style_partno,v_qty,giftcard,TCPOutOfStockFlagUSStore,TCPFit,list_of_attributes,product_name,imagename,productid,uniqueId,TCPBazaarVoiceReviewCount,product_short_description,min_list_price,min_offer_price,prodpartno,TCPOutOfStockFlagUSStore,numberOfProducts&pagetype=boolean&uid=uid-1556134826734-57623&p-id=categoryPathId:';
  } else {
    url = 'https://search.unbxd.io/' + apiKeys[env][apiKey] + '/' + apiKeys[env][siteKey] + '/search?start=0&rows=200&variants=true&variants.count=100&' + oosList + '&version=V2&fields=alt_img,style_partno,v_qty,TCPOutOfStockFlagUSStore,list_of_attributes,giftcard,TCPFit,product_name,imagename,productid,product_short_description,min_list_price,min_offer_price,prodpartno,TCPOutOfStockFlagUSStore,numberOfProducts&q=';
  }
  return url;
}

$(window).scroll(function () {
  var height = $(window).scrollTop();
  if (height > 100) {
    $('#back2Top').fadeIn();
  } else {
    $('#back2Top').fadeOut();
  }
});

let timeout;

function addShakeEffect() {
  $(".grid-square").hover(function (e) {
    /* Mouse enter */
    const self = $(this);
    timeout = setTimeout(function () {
      $(".grid-square").removeClass("shake-tile");
      self.addClass("shake-tile");
    }, 1500);
  }, function () {
    /* Mouse leave */
    clearTimeout(timeout);
    $(".grid-square").removeClass("shake-tile");

  });
}

function randombg() {
  var random = Math.floor(Math.random() * 7) + 0;
  var bigSize = ["url('../images/create.jpg')",
    "url('../images/bulb.jpg')",
    "url('../images/1.jpg')",
    "url('../images/2.jpg')",
    "url('../images/3.jpg')",
    "url('../images/4.jpg')",
    "url('../images/5.jpg')",
    "url('../images/sky.jpg')",
  ]
  $('body').css({
    backgroundImage: bigSize[random]
  });
}
